
// Middlewares to use.

var express = require('express');
var http = require('http');
var fs = require('fs');

var morgan = require('morgan');

// Pick up the settings files
var app = express();
var App = require('./config/app');
//var DB = require('./config/db');
var AppVars = require('./config/vars');

var routes = require('./routes/main');

var args = process.argv;
var env = process.env;

// Default to development environment.
if (!args[2]) {
    App.ENV = 'local';
} else {
    App.ENV = args[2];
}


//console.log(GeneralVars)


var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies




app.set('port', App.getVars().port);




app.use(morgan('dev'));




http.createServer(app).listen(app.get('port'), function () {
    console.log(App.ENV, 'Base server listening on port ', app.get('port'));
});
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    console.log("Request Parameters: ", req.body);
    next();
});

app.post('/shops', routes.getShops);
app.post('/addbetaperson', routes.addBetaPerson)
app.all('/', routes.index);


