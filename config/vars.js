var env = process.env;

module.exports = {
    local: {
        port: 80,
        ip: "https://shops-prod.greenjinn.com",
        credentials: { // test credentials
            notSureWhatYet: "blabla",
            fcm_server_key: "blabla"
        },
        redis: {  
            port      : env.REDIS_PORT,
            host      : env.REDIS_HOST,
        }
    },
    staging: {
        port: 4001,
        ip: "http://users-staging.greenjinn.com",
        credentials: { // test credentials
            notSureWhatYet: "blabla",
            fcm_server_key: "blabla"
        }
    },
    production: {
        port: 80,
        ip: "https://shops-prod.greenjinn.com",
        credentials: { // test credentials
            notSureWhatYet: "blabla",
            fcm_server_key: "blabla"
        }
    }
};
