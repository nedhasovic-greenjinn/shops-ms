//var db = require('./db');
var vars = require('./vars');

var All = {
    'vars': vars
};

var App = {

    getVars: function() {
        return App.getSettingsOf('vars');
    },
    getSettingsOf: function(what) {
        if (!App.ENV || !All[what]) {
            console.log('Getting some settings');
            return null;
        }
        return All[what][App.ENV];
    }
};

module.exports = App;
