FROM mhart/alpine-node:10
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci --prod
RUN npm i -g nodemon


# Only copy over the node pieces we need from the above image
#FROM alpine:3.7
#COPY --from=0 /usr/bin/node /usr/bin/
#COPY --from=0 /usr/lib/libgcc* /usr/lib/libstdc* /usr/lib/
#WORKDIR /app
COPY ./ /app
#COPY --from=0 /app .
#COPY . .
EXPOSE 4000
#CMD nodemon index.js
CMD node index.js
#CMD npm run dev
