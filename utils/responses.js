var _ = require('lodash');

function sendErrorFlagResponse(flag, tracking_id, err_message, res) {
    var response = {
        is_error: 1,
        flag: flag,
        tracking_id: tracking_id,
        err: err_message
    };
    console.log("Response ", response);
    res.send(JSON.stringify(response));
    var sql = "INSERT INTO `error_logs` (`tracking_id`, `error`, `flag`, `extra_info`, `req_id`, `error_time`) values(?,?,?,?,?,NOW())";
    // connection.query(sql, [tracking_id, JSON.stringify(err_message), flag, '', res.req_id || 0], function (error, result) {
    //     console.log(error);
    // });
};
exports.sendErrorFlagResponse = sendErrorFlagResponse;

function sendSuccessFlagResponse(flag, res) {
    var response = {
        is_error: 0,
        flag: flag
    };
    console.log("Response ", response);
    res.send(JSON.stringify(response));
};
exports.sendSuccessFlagResponse = sendSuccessFlagResponse;

function sendSuccessFlagResponseWithData(flag, data, res) {
    var response = {
        is_error: 0,
        flag: flag
    };
    console.log("Response ", response);
    res.send(JSON.stringify(_.extend(response, data)));
};
exports.sendSuccessFlagResponseWithData = sendSuccessFlagResponseWithData;

function sendErrorFlagResponseWithData(flag, tracking_id, err_message, data, res) {
    var response = {
        is_error: 1,
        flag: flag,
        tracking_id: tracking_id,
        err: err_message
    };
    console.log("Response ", response);
    res.send(JSON.stringify(_.extend(response, data)));
    var sql = "INSERT INTO `error_logs` (`tracking_id`, `error`, `flag`, `extra_info`, `req_id`, `error_time`) VALUES (?,?,?,?,?,NOW())";
    // connection.query(sql, [tracking_id, JSON.stringify(err_message), flag, JSON.stringify(data), res.req_id || 0], function (error, result) {});
};
exports.sendErrorFlagResponseWithData = sendErrorFlagResponseWithData;

exports.sendValidationError = function (err, tracking_number, res) {
    switch (parseInt(err.type)) {
    case 1:
        sendErrorFlagResponse(39, tracking_number + err.type, err.key + " is not valid email.", res);
        break;
    case 2:
        sendErrorFlagResponse(3, tracking_number + err.type, err.key + " is not valid string.", res);
        break;
    case 3:
        sendErrorFlagResponse(7, tracking_number + err.type, err.key + " is not valid mobile.", res);
        break;
    case 4:
        sendErrorFlagResponse(3, tracking_number + err.type, err.key + " is not valid number.", res);
        break;
    case 5:
        sendErrorFlagResponse(40, tracking_number + err.type, err.key + " is not valid postcode.", res);
        break;
    case 6:
        sendErrorFlagResponse(41, tracking_number + err.type, err.key + " is not valid date.", res);
        break;
    case 7:
        sendErrorFlagResponse(3, tracking_number + err.type, err.key + " is not valid device_type.", res);
        break;
    case 8:
        sendErrorFlagResponse(3, tracking_number + err.type, err.key + " is not valid address object.", res);
        break;
    case 9:
        sendErrorFlagResponse(42, tracking_number + err.type, err.error, res);
        break;
    default:
        sendErrorFlagResponse(3, tracking_number + err.type, err.key + " Default case", res);
    }
};
