var sendResponse = require('./responses');
var async = require('async');

function checkBlankObject(obj, callback) {
    var blankArray = [];
    async.forEachOf(obj, function (obj_prop, key, callback1) {
        if (obj_prop.required == 1 && obj_prop.value === '') {
            blankArray.push(key + " is empty string");
            callback1();
        } else if (obj_prop.required == 1 && obj_prop.value === undefined) {
            blankArray.push(key + " is undefined");
            callback1();
        } else if (obj_prop.required == 1 && obj_prop.value === null) {
            blankArray.push(key + " is null");
            callback1();
        } else if (obj_prop.required == 1 && !obj_prop.value && obj_prop.value != 0) {
            blankArray.push(key + " is not defined");
            callback1();
        } else {
            callback1();
        }
    }, function (err) {
        callback(blankArray);
    });
};
exports.checkBlankObject = checkBlankObject;

function checkValidations(obj, callback) {
    var validateArray = [];
    async.forEachOf(obj, function (obj_prop, key, callback1) {
        if (obj_prop.required == 1 && obj_prop.is_email == 1 && !validateEmail(obj_prop.value)) {
            validateArray.push(key + " is not valid email.");
            callback1({
                key: key,
                type: 1
            });
        } else if (obj_prop.required == 1 && obj_prop.is_string == 1 && !validateString(obj_prop.value)) {
            validateArray.push(key + " is not valid string.");
            callback1({
                key: key,
                type: 2
            });
        } else if (obj_prop.required == 1 && obj_prop.is_mobile == 1 && !validateMobile(obj_prop.value)) {
            validateArray.push(key + " is not valid mobile.");
            callback1({
                key: key,
                type: 3
            });
        } else if (obj_prop.required == 1 && obj_prop.is_number == 1 && !isNumeric(obj_prop.value)) {
            validateArray.push(key + " is not valid number.");
            callback1({
                key: key,
                type: 4
            });
        } else if (obj_prop.required == 1 && obj_prop.is_postcode == 1 && !validatePostCode(obj_prop.value)) {
            validateArray.push(key + " is not valid postcode.");
            callback1({
                key: key,
                type: 5
            });
        } else if (obj_prop.required == 1 && obj_prop.is_date == 1 && !validateDate(obj_prop.value)) {
            validateArray.push(key + " is not valid date.");
            callback1({
                key: key,
                type: 6
            });
        } else if (obj_prop.required == 1 && obj_prop.is_device_type == 1 && !validateDeviceType(obj_prop.value)) {
            validateArray.push(key + " is not valid device Type.");
            callback1({
                key: key,
                type: 7
            });
        } else if (obj_prop.required == 1 && obj_prop.is_address == 1 && !validateAddress(obj_prop.value)) {
            validateArray.push(key + " is not valid address object.");
            callback1({
                key: key,
                type: 8
            });
        } else if (obj_prop.required == 1 && obj_prop.is_price_array == 1) {
            validatePricingArray(obj_prop.value, function (valid_object) {
                if (valid_object.hasOwnProperty('is_valid')) {
                    callback1();
                } else {
                    validateArray.push(key + " is not valid pricing_array.");
                    callback1({
                        key: key,
                        type: 9,
                        error: valid_object.error
                    });
                }
            });
        } else {
            callback1();
        }
    }, function (err) {
        if (err) {
            callback(err, validateArray);
        } else {
            callback(null, validateArray);
        }
    });
};

exports.checkValidations = checkValidations;

function checkBlankValue(res, body, tracking_number, callback) {
    checkBlankObject(body, function (checkBlankData) {
        if (checkBlankData.length) {
            sendResponse.sendErrorFlagResponseWithData(1, tracking_number + 'a', checkBlankData, {
                value_missing: checkBlankData
            }, res);
        } else {
            checkValidations(body, function (err, validate) {
                if (err) {
                    sendResponse.sendValidationError(err, tracking_number + 'b', res);
                } else {
                    if (validate.length) {
                        sendResponse.sendErrorFlagResponseWithData(2, tracking_number + 'c', validate, response, res);
                    } else {
                        callback(null);
                    }
                }
            });

        }
    });
};
exports.checkBlankValue = checkBlankValue;

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

function validateString(password) {
    if (password.trim().length == 0) {
        return false;
    } else if (typeof password === 'string') {
        return true;
    } else {
        return false;
    }
};

function validateMobile(mobile) {
    var mobile_first = mobile.split("-")[0];
    var mobile_second = mobile.split("-")[1];
    if (mobile.trim().length == 0) {
        return false;
    } else if (mobile_first.charAt(0) != "+") {
        return false;
    } else if (mobile_second.length < 9) {
        return false;
    } else if (mobile_first.length == 1) {
        return false;
    } else {
        return true;
    }
};

function validatePostCode(postcode) {
    if (postcode.toString().trim().length == 0) {
        return false;
    } else if (postcode.toString().length < 4 || postcode.toString().length > 8) {
        return false;
    } else {
        return true;
    }
};

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};

function validateDate(date) {
    if (date.split('-').length != 3) {
        return false;
    } else if (date.length != 10) {
        return false;
    } else {
        return true;
    }
};

function validateDeviceType(device_type) {
    if (device_type == 0 || device_type == 1 || device_type == 2) {
        return true;
    } else {
        return false;
    }
};

function validateAddress(yourVariable) {
    if (yourVariable !== null && typeof yourVariable === 'object') {
        if (yourVariable.hasOwnProperty('line1') && yourVariable.hasOwnProperty('city') && yourVariable.hasOwnProperty('postal_code') && yourVariable.hasOwnProperty('state')) {
            return true;
        } else {
            return false;
        }
    } else if (JSON.parse(yourVariable) !== null && typeof JSON.parse(yourVariable) === 'object') {
        var address = JSON.parse(yourVariable)
        if (address.hasOwnProperty('line1') && address.hasOwnProperty('city') && address.hasOwnProperty('postal_code') && address.hasOwnProperty('state')) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
};

function validatePricingArray(pricing_array, callback_main) {
    if (pricing_array.length != 24) {
        callback_main({
            is_invalid: 1,
            error: "Length not equal to 24"
        });
    } else {
        async.forEachOf(pricing_array, function (item, key, callback1) {
            if (isNumeric(item) && item <= 100 && item >= 0) {
                callback1();
            } else {
                callback1(key + " item is not valid");
            }
        }, function (err) {
            if (err) {
                callback_main({
                    is_invalid: 1,
                    error: err
                });
            } else {
                callback_main({
                    is_valid: 1
                });
            }
        });
    }
};

exports.checkAppVersion = function (device_type, app_type, app_version, server_type, res, callback) {
    var sql = "SELECT `id`, `device_type`, `app_version`, `critical`, `last_critical`, `app_type`, `ip_port` FROM `app_version` WHERE `device_type` = ? AND `app_type` = ? AND `server_type` = ? LIMIT 1";
    connection.query(sql, [device_type, app_type, server_type], function (err, response) {
        console.log(response);
        app_version = parseInt(app_version);
        if (err || !response || !response[0]) {
            res.send({
                is_error: 1,
                error: "Something Went Wrong.",
                show: 1,
                err: err
            });
        } else if (response && response[0] && response[0].app_version > app_version) {
            if (response[0].last_critical >= app_version) {
                res.send({
                    is_error: 1,
                    error: "Update app with newer version.",
                    show_popup: 1,
                    flag: 12,
                    is_force: 1,
                    update_message: "Update app with newer version."
                });
            } else if (response[0].critical == 1) {
                res.send({
                    is_error: 1,
                    error: "Update app with newer version.",
                    show_popup: 1,
                    flag: 12,
                    is_force: 1,
                    update_message: "Update app with newer version."
                });
            } else {
                callback(null, response, "Update app version.");
            }
        } else {
            callback(null, response, 0);
        }
    });
};
