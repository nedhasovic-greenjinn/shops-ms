var AppVars = require('./../config/vars');
var App = require('./../config/app');
var env = process.env;
//console.log(env);

var main = {

    getShops: function(req, res) {

      //console.log(req.body.email);

      main.checkIfBeta(req, res);
    },
    addBetaPerson: function(req, res) {

        var redis = require("redis"),
        client = redis.createClient({
            port      : App.getVars().redis.port,
            host      : App.getVars().redis.host,
          });

      client.on("error", function (err) {
          console.log("Error " + err);
       });

        console.log('adding beta person');
        var email = req.body.email.toLowerCase();
        console.log('Email we really added: '+email);
        client.set(email, "1", function(err,reply) {
        if(!err) {
                  res.writeHead(200, {'content-type': 'text/html'});
                  res.end(
                     'User Added.'
                  );

        }else{

        }

      });

    },
    checkIfBeta: function(req, res){

      res.writeHead(200, {'content-type': 'text/html'});
      var redis = require("redis"),
        client = redis.createClient({
            port      : App.getVars().redis.port,
            host      : App.getVars().redis.host,
          });

      client.on("error", function (err) {
          console.log("Error " + err);
       });
      console.log('checking for beta person');
      var email = req.body.email.toLowerCase();
      client.exists(email, function(err,reply) {
        if(!err) {
          
          if(reply === 1) {
            
            var shops = {
            shops: [
                {
                  id: 4,
                  name: "Iceland",
                  priority: 400
                },
                {
                  id: 3,
                  name: "Tesco",
                  priority: 300
                },
                {
                  id: 2,
                  name: "Waitrose",
                  priority: 100
                },
                {
                  id: 1,
                  name: "Sainsbury's",
                  priority: 200
                }
              ]
          };
            console.log('it exists')
            res.end(JSON.stringify(shops));
            //var reply true;

          }
          else {
            console.log('it does not exists')

            var shops = {
                    shops: [
                        {
                          id: 3,
                          name: "Tesco",
                          priority: 300
                        },
                        {
                          id: 2,
                          name: "Waitrose",
                          priority: 100
                        },
                        {
                          id: 1,
                          name: "Sainsbury's",
                          priority: 200
                        }
                      ]
                  };
              res.end(JSON.stringify(shops));

          }

        }
        
      }


      );
    },
    index: function(req, res){
        res.writeHead(200, {'content-type': 'text/html'});
        res.end(
           '<html>' +
           '<div>Shops Microservice OK</div>' +
           '</html>'
        );
    }
};


module.exports = main;